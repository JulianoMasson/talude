#include "AboutPanel.h"

#include <fstream>
#include <string>

#include <wx/wx.h>
#include <wx/log.h>
#include <wx/progdlg.h>

BEGIN_EVENT_TABLE(AboutPanel, wxPanel)
EVT_PAINT(AboutPanel::paintEvent)
END_EVENT_TABLE()


AboutPanel::AboutPanel(wxFrame* parent) : wxPanel(parent)
{
	// load the file... ideally add a check to see if loading was successful
	image.LoadFile("BITMAP_ABOUT_PANEL", wxBITMAP_TYPE_BMP_RESOURCE);
	
	versionText << "Vers�o " << TALUDE_VERSION_MAJOR << "." << TALUDE_VERSION_MINOR << "." << TALUDE_VERSION_PATCH;
	
	this->Layout();
	this->Fit();
	this->Centre(wxBOTH);
}
/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */
void AboutPanel::paintEvent(wxPaintEvent & evt)
{
	// depending on your system you may need to look at double-buffered dcs
	wxPaintDC dc(this);
	render(dc);
}

/*
 * Alternatively, you can use a clientDC to paint on the panel
 * at any time. Using this generally does not free you from
 * catching paint events, since it is possible that e.g. the window
 * manager throws away your drawing when the window comes to the
 * background, and expects you will redraw it when the window comes
 * back (by sending a paint event).
 */
void AboutPanel::paintNow()
{
	// depending on your system you may need to look at double-buffered dcs
	wxClientDC dc(this);
	render(dc);
}

/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void AboutPanel::render(wxDC&  dc)
{
	dc.DrawBitmap(image, 0, 0, false);
	dc.SetBackgroundMode(wxTRANSPARENT);
	dc.SetFont(wxFont(24, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_HEAVY, false, wxT("Arial")));
	dc.SetTextForeground(wxColour(147, 126, 104));
	dc.DrawText(versionText, 582, 162);
	dc.SetTextForeground(wxColour(255, 255, 255));
	dc.DrawText(versionText, 580, 160);
}
