#pragma once
#include <vector>
#include <vtkSmartPointer.h>

class vtkPoints;
class vtkPolyData;

class DisplacementTool
{
public:
	DisplacementTool() : testPointCloud(nullptr), referencePointCloud(nullptr), referenceSurface(nullptr), datFile("") {};
	DisplacementTool(vtkSmartPointer<vtkPoints> testPointCloud, vtkSmartPointer<vtkPoints> referencePointCloud, vtkSmartPointer<vtkPolyData> referenceSurface,
		const std::string& datFilePath) : testPointCloud(testPointCloud), referencePointCloud(referencePointCloud),
		referenceSurface(referenceSurface), datFile(datFilePath) {};
	~DisplacementTool()
	{
		testPointCloud = nullptr;
		referencePointCloud = nullptr;
		referenceSurface = nullptr;
	};

	//Process
	bool Compute(const std::string& displacementFile, const std::string& tensionsFile);

private:
	vtkSmartPointer<vtkPoints> testPointCloud;
	vtkSmartPointer<vtkPoints> referencePointCloud;
	vtkSmartPointer<vtkPolyData> referenceSurface;
	std::string datFile;

	bool WriteDatFile(const std::string& filename, const std::vector<double*>& axisDistances, bool computePP);
	bool ExecuteCPFL(const std::string& inputDatFile, const std::string& outputDatFile);
	//Load the .dat result from stress3D_CPFL.exe
	vtkSmartPointer<vtkPolyData> LoadDatFile(const std::string& datFile);

};