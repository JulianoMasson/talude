#include "PLYReader.h"

#include <sstream>
#include <map>

#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>

#include <wx/log.h>
#include <wx/file.h>

//Read some T value inside a binary file
template <class T>
static T readBin(std::ifstream &in)
{
	T val;
	in.read(reinterpret_cast<char*>(&val), sizeof(T));
	return val;
}

struct Property
{
	Property(const std::string& name, const unsigned int type, const unsigned int line_poistion, const unsigned int list_size_type = 0)
		: name(name), type(type), line_poistion(line_poistion), list_size_type(list_size_type) {};
	const std::string name;
	const unsigned int type;
	const unsigned int line_poistion;
	const unsigned int list_size_type;
};

struct Element
{
	Element(const std::string& name, const unsigned int& number_of_elements)
		: name(name), number_of_elements(number_of_elements) {};
	const std::string name;
	const unsigned int number_of_elements;
	std::vector<Property> properties;

	bool has_property(const std::string& name) const
	{
		for (const auto& prop : properties)
		{
			if (prop.name == name)
			{
				return true;
			}
		}
		return false;
	}

	Property* get_property(const std::string& name)
	{
		for (size_t i = 0; i < properties.size(); i++)
		{
			if (properties[i].name == name)
			{
				return &properties[i];
			}
		}
		return nullptr;
	}

};

struct Header
{
	std::string format;
	std::string version;
	std::vector<Element> elements;

	bool has_property(const std::string& name) const
	{
		for (const auto& element : elements)
		{
			if (element.has_property(name))
			{
				return true;
			}
		}
		return false;
	}

	Element* get_element(const std::string& name)
	{
		for (size_t i = 0; i < elements.size(); i++)
		{
			if (elements[i].name == name)
			{
				return &elements[i];
			}
		}
		return nullptr;
	}
};

void PLYReader::ignore_property(std::ifstream & in, const Property & prop)
{
	if (prop.list_size_type)
	{
		//It is a list, list properties are not supported for vertex
		auto number_of_itens_list = get_item_value(in, prop.list_size_type);
		for (size_t j = 0; j < number_of_itens_list; j++)
		{
			get_item_value(in, prop.type);
		}
	}
	else
	{
		get_item_value(in, prop.type);
	}
}

unsigned int PLYReader::find_type(const std::string& type)
{
	if (type == "char" || type == "int8")
	{
		return VTK_CHAR;
	}
	else if (type == "uchar" || type == "uint8")
	{
		return VTK_UNSIGNED_CHAR;
	}
	else if (type == "short" || type == "int16")
	{
		return VTK_SHORT;
	}
	else if (type == "ushort" || type == "uint16")
	{
		return VTK_UNSIGNED_SHORT;
	}
	else if (type == "int" || type == "int32")
	{
		return VTK_INT;
	}
	else if (type == "uint" || type == "uint32")
	{
		return VTK_UNSIGNED_INT;
	}
	else if (type == "float" || type == "float32")
	{
		return VTK_FLOAT;
	}
	else if (type == "double")
	{
		return VTK_DOUBLE;
	}
	return 0;
}

double PLYReader::get_item_value(std::ifstream& in, int type)
{
	if (isASCII)
	{
		double temp;
		in >> temp;
		return temp;
	}
	switch (type)
	{
	case VTK_CHAR:
	{
		return ((double)readBin<vtkTypeInt8>(in));
	}
	case VTK_UNSIGNED_CHAR:
	{
		return ((double)readBin<vtkTypeUInt8>(in));
	}
	case VTK_SHORT:
	{
		return ((double)readBin<vtkTypeInt16>(in));
	}
	case VTK_UNSIGNED_SHORT:
	{
		return ((double)readBin<vtkTypeUInt16>(in));
	}
	case VTK_INT:
	{
		return ((double)readBin<vtkTypeInt32>(in));
	}
	case VTK_UNSIGNED_INT:
	{
		return ((double)readBin<vtkTypeUInt32>(in));
	}
	case VTK_FLOAT:
	{
		return ((double)readBin<vtkTypeFloat32>(in));
	}
	case VTK_DOUBLE:
	{
		return readBin<vtkTypeFloat64>(in);
	}
	}
	wxLogError("get_item_value: bad type");
	return 0;
}

vtkSmartPointer<vtkPolyData> PLYReader::read(const std::string& filename)
{
	if (!wxFileExists(filename))
	{
		return nullptr;
	}
	//Read header
	Header plyHeader;
	if (!readHeader(filename, plyHeader))
	{
		return nullptr;
	}
	//Vertex
	Element* element_vertex = plyHeader.get_element("vertex");
	if (!element_vertex)
	{
		return nullptr;
	}
	if (!plyHeader.has_property("x") || !plyHeader.has_property("y") || !plyHeader.has_property("z"))
	{
		return nullptr;
	}
	const auto pointType = element_vertex->get_property("x")->type;
	//Used to test if we have additional data
	unsigned int vertex_properties_size = 3;
	vtkNew<vtkPoints> points;
	//Points need to be float, since the majority of vtk filters do not support double
	//points->SetDataType(element_vertex->get_property("x")->type);
	//Normals
	vtkSmartPointer<vtkFloatArray> normals = nullptr;
	auto normalType = 0.0;
	auto linePositionNormal = 500;
	if (plyHeader.has_property("nx"))
	{
		normalType = element_vertex->get_property("nx")->type;
		linePositionNormal = element_vertex->get_property("nx")->line_poistion;
		normals = vtkSmartPointer<vtkFloatArray>::New();
		normals->SetNumberOfComponents(3);
		normals->SetName("Normals");
		vertex_properties_size += 3;
	}
	//Color
	vtkSmartPointer<vtkUnsignedCharArray> colors = nullptr;
	const bool hasAlpha = (plyHeader.has_property("a") || plyHeader.has_property("alpha"));
	auto colorType = 0.0;
	auto linePositionColor = 500;
	if ((plyHeader.has_property("r") || plyHeader.has_property("red") || plyHeader.has_property("diffuse_red")))
	{
		if (plyHeader.has_property("r"))
		{
			colorType = element_vertex->get_property("r")->type;
			linePositionColor = element_vertex->get_property("r")->line_poistion;
		}
		else if (plyHeader.has_property("red"))
		{
			colorType = element_vertex->get_property("red")->type;
			linePositionColor = element_vertex->get_property("red")->line_poistion;
		}
		else if (plyHeader.has_property("diffuse_red"))
		{
			colorType = element_vertex->get_property("diffuse_red")->type;
			linePositionColor = element_vertex->get_property("diffuse_red")->line_poistion;
		}
		colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
		if (hasAlpha)
		{
			colors->SetNumberOfComponents(4);
			colors->SetName("RGBA");
			vertex_properties_size += 4;
		}
		else
		{
			colors->SetNumberOfComponents(3);
			colors->SetName("RGB");
			vertex_properties_size += 3;
		}
	}
	//Additional data
	std::map< std::string, vtkSmartPointer<vtkDataArray>  > additional_properties_vertex;
	for (size_t i = vertex_properties_size; i < element_vertex->properties.size(); i++)
	{
		// We do not support lists
		if (element_vertex->properties[i].list_size_type != 0)
		{
			continue;
		}
		vtkSmartPointer<vtkDataArray> data_array = vtkDataArray::CreateDataArray(element_vertex->properties[i].type);
		data_array->SetNumberOfComponents(1);
		data_array->SetName(element_vertex->properties[i].name.c_str());
		additional_properties_vertex.emplace( element_vertex->properties[i].name, data_array);
	}
	//Face
	vtkNew<vtkIdTypeArray> polygonsOffsets;
	vtkNew<vtkIdTypeArray> polygonsConnectivity;
	Element* element_face = plyHeader.get_element("face");
	auto listVertexType = 0.0;
	auto vertexType = 0.0;
	if (element_face)
	{
		if (element_face->has_property("vertex_index"))
		{
			listVertexType = element_face->get_property("vertex_index")->list_size_type;
			vertexType = element_face->get_property("vertex_index")->type;
		}
		else if (element_face->has_property("vertex_indices"))
		{
			listVertexType = element_face->get_property("vertex_indices")->list_size_type;
			vertexType = element_face->get_property("vertex_indices")->type;
		}
		polygonsOffsets->SetNumberOfComponents(1);
		polygonsConnectivity->SetNumberOfComponents(1);
	}
	//Load data
	std::ifstream plyFile;
	if (isASCII)
	{
		plyFile.open(filename);
	}
	else
	{
		plyFile.open(filename, ios::binary);
	}
	if (!plyFile.is_open())
	{
		return nullptr;
	}
	std::string line;
	//Skip header
	while (std::getline(plyFile, line) && line != "end_header" && line != "end_header\n" && line != "end_header\r") {}
	const bool normalFirst = (linePositionNormal < linePositionColor);
	for (const auto& element : plyHeader.elements)
	{
		const auto number_of_elements = element.number_of_elements;
		if (element.name == "vertex")
		{
			for (size_t i = 0; i < number_of_elements; i++)
			{
				double point[3], normal[3], color[4];
				for (size_t j = 0; j < 3; j++)
				{
					point[j] = get_item_value(plyFile, pointType);
				}
				if (normalFirst)
				{
					if (normals)
					{
						for (size_t j = 0; j < 3; j++)
						{
							normal[j] = get_item_value(plyFile, normalType);
						}
					}
					if (colors)
					{
						for (size_t j = 0; j < 3; j++)
						{
							color[j] = get_item_value(plyFile, colorType);
						}
						if (hasAlpha)
						{
							color[3] = get_item_value(plyFile, colorType);
						}
					}
				}
				else
				{
					if (colors)
					{
						for (size_t j = 0; j < 3; j++)
						{
							color[j] = get_item_value(plyFile, colorType);
						}
						if (hasAlpha)
						{
							color[3] = get_item_value(plyFile, colorType);
						}
					}
					if (normals)
					{
						for (size_t j = 0; j < 3; j++)
						{
							normal[j] = get_item_value(plyFile, normalType);
						}
					}
				}

				// Additional data
				for (size_t j = vertex_properties_size; j < element_vertex->properties.size(); j++)
				{
					const auto prop = element_vertex->properties[j];
					auto iterator = additional_properties_vertex.find(prop.name);
					if (iterator != additional_properties_vertex.end())
					{
						iterator->second->InsertNextTuple1(get_item_value(plyFile, prop.type));
					}
					else
					{
						//It is a list, list properties are not supported for vertex
						ignore_property(plyFile, prop);
					}
				}
				points->InsertNextPoint(point);
				if (normals)
				{
					normals->InsertNextTuple(normal);
				}
				if (colors)
				{
					if (hasAlpha)
					{
						colors->InsertNextTuple4(color[0], color[1], color[2], color[3]);
					}
					else
					{
						colors->InsertNextTuple3(color[0], color[1], color[2]);
					}
				}
			}
		}
		else if (element.name == "face")
		{
			for (size_t i = 0; i < number_of_elements; i++)
			{
				for (size_t j = 0; j < element_face->properties.size(); j++)
				{
					if (j == 0)
					{
						//Get the number of indexes
						const auto number_of_indexes = get_item_value(plyFile, listVertexType);
						polygonsOffsets->InsertNextTuple1(polygonsConnectivity->GetNumberOfTuples());
						for (size_t k = 0; k < number_of_indexes; k++)
						{
							polygonsConnectivity->InsertNextTuple1(get_item_value(plyFile, vertexType));
						}
					}
					else
					{
						ignore_property(plyFile, element_face->properties[j]);
					}
				}
			}
		}
	}
	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(points);
	if (normals)
	{
		polyData->GetPointData()->SetNormals(normals);
	}
	if (colors)
	{
		polyData->GetPointData()->SetScalars(colors);
	}
	for (const auto& additiona_array : additional_properties_vertex)
	{
		polyData->GetPointData()->AddArray(additiona_array.second);
	}
	if (element_face)
	{
		//Add the last offset position
		polygonsOffsets->InsertNextTuple1(polygonsConnectivity->GetNumberOfTuples());
		vtkNew<vtkCellArray> polygons;
		polygons->SetData(polygonsOffsets, polygonsConnectivity);
		polyData->SetPolys(polygons);
	}
	plyFile.close();
	return polyData;
}

bool PLYReader::readHeader(const std::string& filename, Header & plyHeader)
{
	std::ifstream plyFile(filename);
	if (!plyFile.is_open())
	{
		return 0;
	}
	std::string line;
	std::getline(plyFile, line);
	if (line != "ply")
	{
		plyFile.close();
		return 0;
	}
	unsigned int property_position = 0;
	while (std::getline(plyFile, line) &&
		line != "end_header")
	{
		//Split the line by space
		std::vector<std::string> tokens;
		std::istringstream iss(line);
		std::string token;
		while (std::getline(iss, token, ' '))
		{
			tokens.push_back(token);
		}
		if (tokens.size() == 0 || tokens[0] == "comment")
		{
			continue;
		}
		if (tokens[0] == "format")
		{
			plyHeader.format = tokens[1];
			plyHeader.version = tokens[2];
			isASCII = (plyHeader.format == "ASCII" || plyHeader.format == "ascii");
		}
		else if (tokens[0] == "element")
		{
			property_position = 0;
			plyHeader.elements.emplace_back(tokens[1], std::stoi(tokens[2]));
		}
		else if (tokens[0] == "property")
		{
			property_position++;
			if (tokens[1] == "list")
			{
				const unsigned int list_size_type = find_type(tokens[2]);
				const unsigned int type = find_type(tokens[3]);
				if (list_size_type == 0 || type == 0)
				{
					plyFile.close();
					return 0;
				}
				plyHeader.elements.back().properties.emplace_back(tokens[4], type, property_position, list_size_type);
			}
			else
			{
				const unsigned int type = find_type(tokens[1]);
				if (type == 0)
				{
					plyFile.close();
					return 0;
				}
				plyHeader.elements.back().properties.emplace_back(tokens[2], type, property_position);
			}
		}
	}
	plyFile.close();
	return 1;
}
