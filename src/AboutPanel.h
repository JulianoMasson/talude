#pragma once
#include <wx/panel.h>
#include <wx/bitmap.h>

class AboutPanel : public wxPanel
{
	wxBitmap image;
	wxString versionText;

public:
	AboutPanel(wxFrame* parent);

private:
	void paintEvent(wxPaintEvent & evt);
	void paintNow();

	void render(wxDC& dc);

	DECLARE_EVENT_TABLE()
};
