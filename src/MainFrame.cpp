#include "MainFrame.h"

//VTK
#include <vtkPolyData.h>

//WX
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/msgdlg.h>
#include <wx/log.h>
#include <wx/checkbox.h>
#include <wx/stattext.h>
#include <wx/filepicker.h>
#include <wx/stdpaths.h>
#include <wx/menu.h>
#include <wx/aboutdlg.h>

#include "PLYReader.h"
#include "DisplacementTool.h"
#include "AboutPanel.h"

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_MENU(MainFrame::idMenuClose, MainFrame::OnMenuClose)
EVT_MENU(MainFrame::idMenuAbout, MainFrame::OnMenuAbout)
END_EVENT_TABLE()

MainFrame::MainFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) :
	wxFrame(parent, id, title, pos, size, style)
{
	this->SetBackgroundColour(wxColour(240, 240, 240));
	this->SetIcon(wxICON(ICON_AAA_PROJECT));
	//Menu
	auto menuBar = new wxMenuBar(0);
	//File
	auto menuFile = new wxMenu();
	menuFile->Append(new wxMenuItem(menuFile, MainFrame::idMenuClose, "Sair"));
	//Help
	auto menuHelp = new wxMenu();
	menuHelp->Append(new wxMenuItem(menuHelp, MainFrame::idMenuAbout, "Sobre..."));

	menuBar->Append(menuFile, "Arquivo");
	menuBar->Append(menuHelp, "Ajuda");

	this->SetMenuBar(menuBar);

	auto bSizer = new wxBoxSizer(wxVERTICAL);

	//Inputs
	wxFlexGridSizer* fgSizerInputs = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizerInputs->SetFlexibleDirection(wxBOTH);
	fgSizerInputs->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizerInputs->Add(new wxStaticText(this, wxID_ANY, "Superf�cie de refer�ncia"), 0, wxALL, 5);
	pickerReferenceSurface = new wxFilePickerCtrl(this, wxID_ANY, wxEmptyString, "Selectione a superf�cie de refer�ncia", "PLY files (*.ply)|*.ply");
	fgSizerInputs->Add(pickerReferenceSurface, 0, wxALL, 5);

	fgSizerInputs->Add(new wxStaticText(this, wxID_ANY, "Nuvem de pontos de refer�ncia"), 0, wxALL, 5);
	pickerReferenceCloud = new wxFilePickerCtrl(this, wxID_ANY, wxEmptyString, "Selectione a nuvem de pontos de refer�ncia", "PLY files (*.ply)|*.ply");
	fgSizerInputs->Add(pickerReferenceCloud, 0, wxALL, 5);

	fgSizerInputs->Add(new wxStaticText(this, wxID_ANY, "Nuvem de pontos de teste"), 0, wxALL, 5);
	pickerTestCloud = new wxFilePickerCtrl(this, wxID_ANY, wxEmptyString, "Selectione a nuvem de pontos de teste", "PLY files (*.ply)|*.ply");
	fgSizerInputs->Add(pickerTestCloud, 0, wxALL, 5);

	bSizer->Add(fgSizerInputs, 0, wxALL, 5);

	//Options
	cbComputeDisplacement = new wxCheckBox(this, wxID_ANY, "Calcular deslocamento");
	cbComputeDisplacement->Bind(wxEVT_CHECKBOX, &MainFrame::OnCbComputeDisplacement, this);
	bSizer->Add(cbComputeDisplacement, 0, wxALL, 5);

	cbComputeTensions = new wxCheckBox(this, wxID_ANY, "Calcular tens�es");
	cbComputeTensions->Bind(wxEVT_CHECKBOX, &MainFrame::OnCbComputeTensions, this);
	bSizer->Add(cbComputeTensions, 0, wxALL, 5);

	//Outputs
	wxFlexGridSizer* fgSizerOutputs = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizerOutputs->SetFlexibleDirection(wxBOTH);
	fgSizerOutputs->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizerOutputs->Add(new wxStaticText(this, wxID_ANY, "Sa�da deslocamento"), 0, wxALL, 5);
	pickerDisplacement = new wxFilePickerCtrl(this, wxID_ANY, wxEmptyString, "Sa�da deslocamento", "PLY files(*.ply) | *.ply",
		wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
	pickerDisplacement->Enable(false);
	fgSizerOutputs->Add(pickerDisplacement, 0, wxALL, 5);

	fgSizerOutputs->Add(new wxStaticText(this, wxID_ANY, "Sa�da tens�es"), 0, wxALL, 5);
	pickerTensions = new wxFilePickerCtrl(this, wxID_ANY, wxEmptyString, "Sa�da tens�es", "PLY files(*.ply) | *.ply",
		wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
	pickerTensions->Enable(false);
	fgSizerOutputs->Add(pickerTensions, 0, wxALL, 5);

	bSizer->Add(fgSizerOutputs, 0, wxALL, 5);

	//Buttons
	auto bSizerButtons = new wxBoxSizer(wxHORIZONTAL);

	btCompute = new wxButton(this, wxID_ANY, "Calcular");
	btCompute->Bind(wxEVT_BUTTON, &MainFrame::OnBtCompute, this);
	btCompute->Disable();
	bSizerButtons->Add(btCompute, 0, wxALL, 5);

	bSizer->Add(bSizerButtons, 0, wxALL, 5);

	this->SetSizer(bSizer);
	this->Layout();
	this->Centre(wxBOTH);
}

MainFrame::~MainFrame()
{
}

void MainFrame::OnMenuClose(wxCommandEvent & event)
{
	this->Close();
}

void MainFrame::OnMenuAbout(wxCommandEvent & event)
{
	if (this->FindWindowById(idAboutFrame))
	{
		return;
	}
	auto aboutFrame = new wxFrame(this, idAboutFrame, "Sobre", wxDefaultPosition,
		wxSize(800, 535), wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN);

	auto sizer = new wxBoxSizer(wxHORIZONTAL);
	auto projectPanel = new AboutPanel(aboutFrame);
	sizer->Add(projectPanel, 1, wxEXPAND);

	aboutFrame->SetSizer(sizer);
	aboutFrame->SetIcon(wxICON(ICON_AAA_PROJECT));
	aboutFrame->Show(true);
}

void MainFrame::OnCbComputeDisplacement(wxCommandEvent & event)
{
	if (cbComputeDisplacement->GetValue())
	{
		pickerDisplacement->Enable();
	}
	else
	{
		pickerDisplacement->Enable(false);
		pickerDisplacement->SetPath("");
	}
	if (cbComputeDisplacement->GetValue() || cbComputeTensions->GetValue())
	{
		btCompute->Enable();
	}
	else
	{
		btCompute->Disable();
	}
}

void MainFrame::OnCbComputeTensions(wxCommandEvent & event)
{
	if (cbComputeTensions->GetValue())
	{
		pickerTensions->Enable();
	}
	else
	{
		pickerTensions->Enable(false);
		pickerTensions->SetPath("");
	}
	if (cbComputeDisplacement->GetValue() || cbComputeTensions->GetValue())
	{
		btCompute->Enable();
	}
	else
	{
		btCompute->Disable();
	}
}

void MainFrame::OnBtCompute(wxCommandEvent & event)
{
	if (pickerReferenceSurface->GetPath() == "" ||
		pickerReferenceCloud->GetPath() == "" ||
		pickerTestCloud->GetPath() == "")
	{
		wxLogError("Defina todas as entradas corretamente!");
		return;
	}
	wxBeginBusyCursor();
	//Load inputs
	PLYReader plyReader;
	auto referenceSurface = plyReader.read(pickerReferenceSurface->GetPath().ToStdString());
	auto referenceCloud = plyReader.read(pickerReferenceCloud->GetPath().ToStdString());
	auto testCloud = plyReader.read(pickerTestCloud->GetPath().ToStdString());
	auto datFile = wxStandardPaths::Get().GetTempDir() + "\\input";

	DisplacementTool displacementTool(testCloud->GetPoints(), referenceCloud->GetPoints(), referenceSurface, datFile.ToStdString());

	auto result = displacementTool.Compute(pickerDisplacement->GetPath().ToStdString(),
		pickerTensions->GetPath().ToStdString());
	if (!result)
	{
		wxLogError("Falha ao gerar o resultado.");
	}
	wxEndBusyCursor();
}
