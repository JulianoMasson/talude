#include "DisplacementTool.h"

#include <sstream>

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkKdTree.h>
#include <vtkNew.h>
#include <vtkIdList.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtkBitArray.h>
#include <vtksys/SystemTools.hxx>

#include <wx/stdpaths.h>
#include <wx/process.h>
#include <wx/log.h>
#include "PLYWriter.h"

bool DisplacementTool::Compute(const std::string & displacementFile, const std::string & tensionsFile)
{
	if (!testPointCloud || !referencePointCloud || !referenceSurface || datFile == "")
	{
		return 0;
	}
	vtkNew<vtkKdTree> kdTree;
	kdTree->BuildLocatorFromPoints(testPointCloud);

	// KdTree used to test if a simplified mesh point is from poisson or not
	vtkNew<vtkKdTree> kdTreePoisson;
	kdTreePoisson->BuildLocatorFromPoints(referencePointCloud);

	const auto numberOfPoints = referenceSurface->GetNumberOfPoints();
	const auto numberOfClosestPoints = 3;
	// Data to be retrived
	std::vector<double*> axisDistances;
	axisDistances.resize(numberOfPoints, nullptr);
	// Add the displacement in the simplified mesh
	vtkNew<vtkFloatArray> displacementArray;
	displacementArray->SetName("Displacement");
	displacementArray->SetNumberOfComponents(1);
	displacementArray->SetNumberOfTuples(numberOfPoints);
	// Add the poisson test in the simplified mesh
	vtkNew<vtkBitArray> poissonArray;
	poissonArray->SetName("PoissonPoint");
	poissonArray->SetNumberOfComponents(1);
	poissonArray->SetNumberOfTuples(numberOfPoints);
	for (int i = 0; i < numberOfPoints; i++)
	{
		double testPoint[3];
		referenceSurface->GetPoint(i, testPoint);
		vtkNew<vtkIdList> result;
		kdTree->FindClosestNPoints(numberOfClosestPoints, testPoint, result);
		double dist2;
		kdTreePoisson->FindClosestPoint(testPoint, dist2);
		if (dist2 <= 1.5)
		{
			poissonArray->SetTuple1(i, 1);
		}
		else
		{
			poissonArray->SetTuple1(i, 0);
		}
		if (result->GetNumberOfIds() < 3)
		{
			continue;
		}
		//Pega os resultados
		double p_res1[3];
		double p_res2[3];
		double p_res3[3];

		testPointCloud->GetPoint(result->GetId(0), p_res1);
		testPointCloud->GetPoint(result->GetId(1), p_res2);
		testPointCloud->GetPoint(result->GetId(2), p_res3);

		// Estima a distancia para cada eixo
		double* axisDistance = new double[3];

		for (int j = 0; j < 3; j++)
		{
			double maxDistance = std::fmax(std::fmax(p_res1[j], p_res2[j]), p_res3[j]);
			double minDistance = std::fmin(std::fmin(p_res1[j], p_res2[j]), p_res3[j]);

			if ((testPoint[j] > minDistance) && (testPoint[j] < maxDistance))
			{
				axisDistance[j] = 0;
			}
			else
			{
				if (testPoint[j] > maxDistance)
				{
					axisDistance[j] = testPoint[j] - maxDistance;
				}
				else
				{
					axisDistance[j] = testPoint[j] - minDistance;
				}

			}
		}
		axisDistances[i] = axisDistance;
		displacementArray->SetTuple1(i, vtkMath::Norm(axisDistance));
	}
	referenceSurface->GetPointData()->AddArray(displacementArray);
	referenceSurface->GetPointData()->AddArray(poissonArray);

	if (displacementFile != "")
	{
		//Save displacement ply
		if (!PLYWriter::Write(displacementFile, referenceSurface, true))
		{
			wxLogError("Erro ao salvar o deslocamento.");
			return 0;
		}
	}
	if (tensionsFile == "")
	{
		return 1;
	}

	bool resultPP = WriteDatFile(datFile + "_PP.dat", axisDistances, true);
	bool resultSFM = WriteDatFile(datFile + "_SFM.dat", axisDistances, false);
	for (vtkIdType i = 0; i < numberOfPoints; i++)
	{
		if (axisDistances[i])
		{
			delete[] axisDistances[i];
		}
	}
	axisDistances.clear();
	if (!resultPP || !resultSFM)
	{
		wxLogError("Erro criando os dats.");
		return 0;
	}
	if (!ExecuteCPFL(datFile + "_PP.dat", datFile + "_PP_output.dat") || !ExecuteCPFL(datFile + "_SFM.dat", datFile + "_SFM_output.dat"))
	{
		wxLogError("Erro ao executar os dats.");
		return 0;
	}
	auto polyDataPP = LoadDatFile(datFile + "_PP_output.dat");
	auto polyDataSFM = LoadDatFile(datFile + "_SFM_output.dat");
	if (!polyDataPP || !polyDataSFM)
	{
		wxLogError("Erro ao carregar os dats.");
		return 0;
	}
	// Merge the two results
	const auto numberOfPointsResultPP = polyDataPP->GetNumberOfPoints();
	const auto numberOfPointsResultSFM = polyDataSFM->GetNumberOfPoints();
	if (numberOfPointsResultPP != numberOfPointsResultSFM)
	{
		wxLogError("Erro na checagem de n�mero de pontos.");
		return 0;
	}
	const auto arrayNames = { "tensionS1", "tensionS2", "tensionS3" };
#pragma omp parallel for
	for (int i = 0; i < numberOfPointsResultPP; i++)
	{
		for (const auto& arrayName : arrayNames)
		{
			polyDataPP->GetPointData()->GetArray(arrayName)->SetTuple1(i,
				polyDataPP->GetPointData()->GetArray(arrayName)->GetTuple1(i) +
				polyDataSFM->GetPointData()->GetArray(arrayName)->GetTuple1(i));
		}
	}
	//Save tensions ply
	if (!PLYWriter::Write(tensionsFile, polyDataPP, true))
	{
		wxLogError("Erro ao salvar as tens�es.");
		return 0;
	}
	return 1;
}

bool DisplacementTool::WriteDatFile(const std::string& filename, const std::vector<double*>& axisDistances, bool computePP)
{
	auto simplifiedSurface = referenceSurface;
	std::ofstream outFileDAT;
	outFileDAT.open(filename);
	if (!outFileDAT.is_open())
	{
		return false;
	}
	const auto numberOfPoints = simplifiedSurface->GetNumberOfPoints();
	const auto numberOfFaces = simplifiedSurface->GetNumberOfCells();
	const auto poissonArray = simplifiedSurface->GetPointData()->GetArray("PoissonPoint");
	outFileDAT << "soil under prescribed surface displacements\n\n" <<
		"1 0 [nsub] [ncopies] \n" <<
		"0\n" << // Tipo de solver adicionar usado modelo. Adicionar como op��o?
		"9.81 0.0 -1.0 0.0 [gmod] [gvec]\n\n" << //Caso decidamos usar um outra dire��o da gravidade modificar
		"2     " << numberOfPoints << "    " << numberOfFaces << "  0 0 "<< computePP << " " << computePP << "\n" <<
		"0  0  0  0.00  0.00  0.00  0.00  0.00  0.00 [ncopx1] [ncopx2] [ncopx3] [disx1] [disx2] [disx3] [rot1] [rot2] [rot3]\n";

	// -------------- Alterar aqui
	outFileDAT << "0    0\n\n";//Numero de pontos da malha de fechamento   / Numero de faces

	for (int i = 0; i < numberOfFaces; i++)
	{
		vtkNew<vtkIdList> cells;
		simplifiedSurface->GetCellPoints(i, cells);
		if (cells->GetNumberOfIds() < 3)
		{
			continue;
		}
		outFileDAT << i + 1 << "\t\t" << cells->GetId(0) + 1 << "\t\t" << cells->GetId(1) + 1 << "\t\t" << cells->GetId(2) + 1 << "\t\t0\t\t0\n";
	}
	outFileDAT << "\n\n\n\n";

	for (int i = 0; i < numberOfPoints; i++)
	{
		double p[3];
		simplifiedSurface->GetPoint(i, p);
		// Escreve ponto
		outFileDAT << i + 1 << "\t0\n";
		outFileDAT << "\t\t\t" << p[0] << "\t\t" << p[1] << "\t\t" << p[2] << "\n";
	}
	outFileDAT << "\n2.0e7, 0.35, 1.8e+3 [E], [v], [density]\n\n";
	outFileDAT << "0    1\n\n" << numberOfFaces << "\n\n";

	for (int i = 0; i < numberOfFaces; i++)
	{
		vtkNew<vtkIdList> cells;
		simplifiedSurface->GetCellPoints(i, cells);
		if (cells->GetNumberOfIds() < 3)
		{
			continue;
		}
		outFileDAT << "\t\t" << i + 1 << "\t\t 3\n";
		const auto numberOfIds = cells->GetNumberOfIds();
		for (int j = 0; j < numberOfIds; j++)
		{
			const auto id = cells->GetId(j);
			if (poissonArray->GetTuple1(id))
			{
				if (computePP)
				{
					outFileDAT << "\t\t" << id + 1 << "\t\t 0.0000000E+00  0.0000000E+00  0.0000000E+00  0  0  0\n";
				}
				else
				{
					outFileDAT << "\t\t" << id + 1 << "\t\t 0.0000000E+00  0.0000000E+00  0.0000000E+00  3  3  3\n";
				}
			}
			else
			{
				outFileDAT << "\t\t" << id + 1 << "\t\t 0.0000000E+00  0.0000000E+00  0.0000000E+00  1  1  1\n";
			}
		}
	}

	if (computePP)
	{
		outFileDAT << "\n0.0\n0.0\n0.0\n\n";
	}
	else
	{
		simplifiedSurface->BuildLinks();
		// Inserir os deslocamentos pelos pontos
		for (int i = 0; i < numberOfPoints; i++)
		{
			// Apenas escrever os pontos com o c�digo 3 3 3
			if (!poissonArray->GetTuple1(i))
			{
				continue;
			}
			vtkNew<vtkIdList> cells;
			simplifiedSurface->GetPointCells(i, cells);
			if (cells->GetNumberOfIds() < 1)
			{
				continue;
			}
			const auto axisDistance = axisDistances[i];
			outFileDAT << "\t\t" << cells->GetId(0) + 1 << "\t\t";
			if (vtkMath::Norm(axisDistance) < 1.0)
			{
				outFileDAT << i + 1 << "\t\t" << axisDistance[0] << "\t\t" << axisDistance[1] << "\t\t" << axisDistance[2] << "\t\t0\t\t0\t\t0\n";
			}
			else
			{
				outFileDAT << i + 1 << "\t\t" << 0 << "\t\t" << 0 << "\t\t" << 0 << "\t\t0\t\t0\t\t0\n";
			}
		}
	}

	outFileDAT << "\n\n\n\n0,4\n";
	outFileDAT.close();
	return true;
}

std::string preparePath(std::string path)
{
	std::replace(path.begin(), path.end(), '\\', '/');
	return "\"" + path + "\"";
}

bool DisplacementTool::ExecuteCPFL(const std::string & inputDatFile, const std::string & outputDatFile)
{
	std::string cpflParameters(preparePath(vtksys::SystemTools::GetFilenamePath(wxStandardPaths::Get().GetExecutablePath().ToStdString()) + "/CPFL/stress3d_cpfl.exe") +
		" " + preparePath(inputDatFile) +
		" " + preparePath(outputDatFile)
	);
	if (wxExecute(cpflParameters, wxEXEC_SYNC) == -1)
	{
		return false;
	}
	if (!wxFileExists(outputDatFile))
	{
		return false;
	}
	return true;
}

vtkSmartPointer<vtkPolyData> DisplacementTool::LoadDatFile(const std::string & datFile)
{
	std::ifstream file(datFile);
	if (!file.is_open())
	{
		return nullptr;
	}
	std::string line;
	unsigned int numPoints = 0;
	unsigned int numFaces = 0;
	// Read the number of element in the header
	while (std::getline(file, line))
	{
		if (line.find("nno        ne      npoi") != std::string::npos)
		{
			std::getline(file, line);
			std::stringstream ss(line);
			unsigned int temp;
			ss >> temp >> numPoints >> numFaces;
			break;
		}
	}
	if (numPoints == 0 || numFaces == 0)
	{
		return nullptr;
	}

	std::vector< std::vector<unsigned int> > faceList;
	faceList.reserve(numFaces);

	vtkNew<vtkPoints> points;
	points->SetNumberOfPoints(numPoints);

	vtkNew<vtkCellArray> polys;

	vtkNew<vtkFloatArray> tensionS1;
	tensionS1->SetName("tensionS1");
	tensionS1->SetNumberOfComponents(1);
	tensionS1->SetNumberOfTuples(numPoints);
	vtkNew<vtkFloatArray> tensionS2;
	tensionS2->SetName("tensionS2");
	tensionS2->SetNumberOfComponents(1);
	tensionS2->SetNumberOfTuples(numPoints);
	vtkNew<vtkFloatArray> tensionS3;
	tensionS3->SetName("tensionS3");
	tensionS3->SetNumberOfComponents(1);
	tensionS3->SetNumberOfTuples(numPoints);
	// We should not have to do this, something is wrong in the assignment of the tensions
#pragma omp parallel for
	for (vtkIdType i = 0; i < numPoints; i++)
	{
		tensionS1->SetTuple1(i, 0.0);
		tensionS2->SetTuple1(i, 0.0);
		tensionS3->SetTuple1(i, 0.0);
	}

	// Read the mesh
	while (std::getline(file, line))
	{
		if (line.find("element") != std::string::npos)
		{
			unsigned int countFaces = 0;
			while (countFaces < numFaces)
			{
				std::getline(file, line);
				unsigned int nFace, c1, c2, c3;
				std::stringstream ss(line);
				ss >> nFace >> c1 >> c2 >> c3;

				std::vector<unsigned int> pointsTemp;
				pointsTemp.reserve(3);
				pointsTemp.emplace_back(c1);
				pointsTemp.emplace_back(c2);
				pointsTemp.emplace_back(c3);

				faceList.emplace_back(pointsTemp);

				vtkIdType idTp[3];
				idTp[0] = c1 - 1;
				idTp[1] = c2 - 1;
				idTp[2] = c3 - 1;
				size_t npts = 3;
				polys->InsertNextCell(3, idTp);
				countFaces++;
			}
			break;
		}
	}
	if (faceList.size() != numFaces)
	{
		return nullptr;
	}
	// Read the points
	unsigned int countPoints = 0;
	while (std::getline(file, line))
	{
		if (line.find("node") != std::string::npos)
		{
			while (countPoints < numPoints)
			{
				std::getline(file, line);
				// Replace all 'D' to 'e'
				std::replace(line.begin(), line.end(), 'D', 'e');
				double s1, s2, s3, s4;
				std::stringstream ss(line);
				ss >> s1 >> s2 >> s3 >> s4;
				points->InsertPoint(countPoints, s2, s3, s4);
				countPoints++;
			}
			break;
		}
	}
	if (countPoints != numPoints)
	{
		return nullptr;
	}
	// Find the principal stresses
	auto principalStress = false;
	while (std::getline(file, line))
	{
		if (line.find("Principal stresses") != std::string::npos)
		{
			principalStress = true;
			break;
		}
	}
	if (!principalStress)
	{
		return nullptr;
	}
	// Read the tensions
	unsigned int countFaces = 0;
	while (std::getline(file, line))
	{
		if (line.find("node") != std::string::npos)
		{
			while (countFaces < numFaces)
			{
				std::getline(file, line);
				if (line.find("ELEM") != std::string::npos)
				{
					for (size_t i = 0; i < 3; i++)
					{
						std::getline(file, line);
						unsigned int index;
						double s1, s2, s3;
						std::stringstream ss(line);
						ss >> index >> s1 >> s2 >> s3;

						unsigned int indexDT = faceList[countFaces][index - 1];
						tensionS1->InsertTuple1(indexDT, s1);
						tensionS2->InsertTuple1(indexDT, s2);
						tensionS3->InsertTuple1(indexDT, s3);
					}
					countFaces++;
				}
			}
			break;
		}
	}
	if (countFaces != numFaces)
	{
		return nullptr;
	}
	file.close();

	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(points);
	polyData->SetPolys(polys);

	polyData->GetPointData()->AddArray(tensionS1);
	polyData->GetPointData()->AddArray(tensionS2);
	polyData->GetPointData()->AddArray(tensionS3);
	return polyData;
}
