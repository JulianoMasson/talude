#pragma once
#include <wx/frame.h>

class wxFilePickerCtrl;
class wxCheckBox;
class wxButton;

class MainFrame : public wxFrame
{
public:
	MainFrame(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(415, 355),
		long style = wxMINIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN);

	~MainFrame();

	//Inputs
	wxFilePickerCtrl* pickerReferenceSurface;
	wxFilePickerCtrl* pickerReferenceCloud;
	wxFilePickerCtrl* pickerTestCloud;
	//Options
	wxCheckBox* cbComputeDisplacement;
	wxCheckBox* cbComputeTensions;
	//Output
	wxFilePickerCtrl* pickerDisplacement;
	wxFilePickerCtrl* pickerTensions;
	//Buttons
	wxButton* btCompute;

private:
	void OnMenuClose(wxCommandEvent& event);
	void OnMenuAbout(wxCommandEvent& event);

	void OnCbComputeDisplacement(wxCommandEvent & event);
	void OnCbComputeTensions(wxCommandEvent & event);
	void OnBtCompute(wxCommandEvent & event);

	DECLARE_EVENT_TABLE()
	enum
	{
		idMenuClose,
		idMenuHelp,
		idMenuAbout,
		idAboutFrame
	};
};