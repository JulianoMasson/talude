#include "PLYWriter.h"

#include <sstream>

#include <vtkDataArray.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>

#include <wx/log.h>

//Write some T value inside a binary file
template <class T>
static void writeBin(std::ofstream &out, T val)
{
	out.write(reinterpret_cast<char*>(&val), sizeof(T));
}

bool PLYWriter::Write(const std::string & filename, vtkSmartPointer<vtkPolyData> polyData, bool hasFaces)
{
	const auto numberOfPoints = polyData->GetNumberOfPoints();
	const auto hasColor = polyData->GetPointData()->GetScalars() != nullptr;
	const auto hasNormals = polyData->GetPointData()->GetNormals() != nullptr;
	std::ofstream outFile(filename, std::ios::binary);
	if (!outFile.is_open())
	{
		wxLogError(wxString("Could not create the ply file " + filename));
		return 0;
	}
	std::vector<unsigned int> pointDataPropertyIndexes;
	Create_header(outFile, pointDataPropertyIndexes, polyData, hasFaces);
	const auto points = polyData->GetPoints();
	for (size_t i = 0; i < numberOfPoints; i++)
	{
		auto point = points->GetPoint(i);
		writeBin<float>(outFile, point[0]);
		writeBin<float>(outFile, point[1]);
		writeBin<float>(outFile, point[2]);
		for (const auto& index : pointDataPropertyIndexes)
		{
			const auto pointDataArray = polyData->GetPointData()->GetArray(index);
			const auto numberOfComponents = pointDataArray->GetNumberOfComponents();
			const auto type = pointDataArray->GetDataType();
			auto tuple = pointDataArray->GetTuple(i);
			for (size_t k = 0; k < numberOfComponents; k++)
			{
				write_value(outFile, type, tuple[k]);
			}
		}
	}
	if (hasFaces)
	{
		const auto numberOfFaces = polyData->GetNumberOfCells();
		unsigned int sizeIds;
		for (size_t i = 0; i < numberOfFaces; i++)
		{
			vtkNew<vtkIdList> idList;
			polyData->GetCellPoints(i, idList);
			sizeIds = idList->GetNumberOfIds();
			writeBin<unsigned char>(outFile, sizeIds);
			for (size_t j = 0; j < sizeIds; j++)
			{
				writeBin<int>(outFile, idList->GetId(j));
			}
		}
	}
	outFile.close();
	return 1;
}

std::string PLYWriter::Get_type(unsigned int type)
{
	switch (type)
	{
	case VTK_CHAR:
		return "char";
	case VTK_UNSIGNED_CHAR:
		return "uchar";
	case VTK_SHORT:
		return "short";
	case VTK_UNSIGNED_SHORT:
		return "ushort";
	case VTK_INT:
		return "int";
	case VTK_FLOAT:
		return "float";
	case VTK_DOUBLE:
		return "double";
	}
	return "";
}

void PLYWriter::write_value(std::ofstream& out, int type, double value)
{
	switch (type)
	{
	case VTK_CHAR:
	{
		writeBin<vtkTypeInt8>(out, value);
		return;
	}
	case VTK_UNSIGNED_CHAR:
	{
		writeBin<vtkTypeUInt8>(out, value);
		return;
	}
	case VTK_SHORT:
	{
		writeBin<vtkTypeInt16>(out, value);
		return;
	}
	case VTK_UNSIGNED_SHORT:
	{
		writeBin<vtkTypeUInt16>(out, value);
		return;
	}
	case VTK_INT:
	{
		writeBin<vtkTypeInt32>(out, value);
		return;
	}
	case VTK_UNSIGNED_INT:
	{
		writeBin<vtkTypeUInt32>(out, value);
		return;
	}
	case VTK_FLOAT:
	{
		writeBin<vtkTypeFloat32>(out, value);
		return;
	}
	case VTK_DOUBLE:
	{
		writeBin<vtkTypeFloat64>(out, value);
		return;
	}
	}
	wxLogError("get_item_value: bad type");
}

void PLYWriter::Create_header(std::ofstream & outFile, std::vector<unsigned int>& pointDataPropertyIndexes,
	vtkSmartPointer<vtkPolyData> polyData, bool hasFaces)
{
	const auto numberOfPoints = polyData->GetNumberOfPoints();
	outFile << "ply" << "\n";
	outFile << "format binary_little_endian 1.0" << "\n";
	outFile << "comment created by SM.A.R.T.3D" << "\n";
	outFile << "element vertex " << numberOfPoints << "\n";
	outFile << "property float x" << "\n";
	outFile << "property float y" << "\n";
	outFile << "property float z" << "\n";
	const auto numberOfArrays = polyData->GetPointData()->GetNumberOfArrays();
	for (size_t i = 0; i < numberOfArrays; i++)
	{
		const auto pointDataArray = polyData->GetPointData()->GetArray(i);
		if (!pointDataArray)
		{
			continue;
		}
		std::string propertyName = pointDataArray->GetName();
		const auto numberOfComponents = pointDataArray->GetNumberOfComponents();
		if (propertyName == "RGB")
		{
			outFile << "property uchar red" << "\n";
			outFile << "property uchar green" << "\n";
			outFile << "property uchar blue" << "\n";
		}
		else if (propertyName == "RGBA")
		{
			outFile << "property uchar red" << "\n";
			outFile << "property uchar green" << "\n";
			outFile << "property uchar blue" << "\n";
			outFile << "property uchar alpha" << "\n";
		}
		else if (propertyName == "Normals")
		{
			outFile << "property float nx" << "\n";
			outFile << "property float ny" << "\n";
			outFile << "property float nz" << "\n";
		}
		else
		{
			const auto propertyType = Get_type(pointDataArray->GetDataType());
			if (propertyType == "")
			{
				continue;
			}
			if (numberOfComponents == 1)
			{
				outFile << "property " << propertyType << " " <<
					propertyName << "\n";
			}
			else
			{
				// We cannot open property lists, so we avoid writing them
				for (size_t j = 0; j < numberOfComponents; j++)
				{
					outFile << "property " << propertyType << " " <<
						propertyName << "_" << j << "\n";
				}
			}
		}
		pointDataPropertyIndexes.emplace_back(i);
	}
	if (hasFaces)
	{
		outFile << "element face " << polyData->GetNumberOfCells() << "\n";
		outFile << "property list uchar int vertex_indices" << "\n";
	}
	outFile << "end_header" << "\n";
}
