#include "App.h"

#include "MainFrame.h"

IMPLEMENT_APP(App)

bool App::OnInit() 
{
	mainFrame = new MainFrame(nullptr, wxID_ANY, "Talude", wxDefaultPosition);
	mainFrame->Show(true);
	this->SetTopWindow(mainFrame);
	return true;
}
